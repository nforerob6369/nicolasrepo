﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BICIEAN.Modelo
{
    public class Persona
    {
        public int Cedula { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Apellido2 { get; set; }
        public int IdTipoPersona { get; set; }
        public string Usuario { get; set; }
        public string Password { get; set; }
        public int Activo { get; set; }

        public Persona(int _Cedula, string _Nombre, string _Apellido, string _Apellido2, int _IdTipoPersona, string _Usuario, string _Password, int _Activo)
        {
            this.Cedula = _Cedula;
            this.Nombre = _Nombre;
            this.Apellido = _Apellido;
            this.Apellido2 = _Apellido2;
            this.IdTipoPersona = _IdTipoPersona;
            this.Usuario = _Usuario;
            this.Password = _Password;
            this.Activo = _Activo;
           
        }
    }
}