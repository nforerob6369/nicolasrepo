﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BiciMasterPage.Master" AutoEventWireup="true" CodeBehind="AdminPage.aspx.cs" Inherits="BICIEAN.AdminPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="col-xs-12" style="padding-left: 75px;">
        <div class="col-xs-12 InformationSection">
            <div class="col-xs-6 form-group">
                <div class="col-xs-12 TituloFormularioPersonalizado">
                    Registro personal
                </div>
                <div id="PersonaForm">
                    <div class="col-xs-12 FomularioPersonalizado">
                        <input type="text" class="form-control" placeholder="Nombre" id="NombrePersona" name="NombrePersona" required>
                    </div>
                    <div class="col-xs-6 FomularioPersonalizado">
                        <input type="text" class="form-control" placeholder="1er Apellido" id="ApellidoPersona" name="ApellidoPersona" required>
                    </div>
                    <div class="col-xs-6 FomularioPersonalizado">
                        <input type="text" class="form-control" placeholder="2do Apellido" id="Apellido2Persona" name="Apellido2Persona" required>
                    </div>
                    <div class="col-xs-12 FomularioPersonalizado">
                        <input type="text" class="form-control" placeholder="Usuario" id="UsuarioPersona" name="UsuarioPersona" required>
                    </div>
                    <div class="col-xs-6 FomularioPersonalizado">
                        <input type="password" class="form-control" placeholder="Contraseña" id="ContrasenaPersona" name="ContrasenaPersona" required>
                    </div>
                    <div class="col-xs-6 FomularioPersonalizado">
                        <input type="password" class="form-control" placeholder="Confirmar Contraseña" id="Contrasena2Persona" name="Contrasena2Persona" required>
                    </div>
                    <div class="col-xs-6 FomularioPersonalizado">
                        <input type="number" class="form-control" placeholder="Cedula" id="CedulaPersona" name="CedulaPersona" required>
                    </div>
                    <div class="col-xs-6 FomularioPersonalizado">
                        <select class="form-control" id="TipoPersona" name="TipoPersona">
                            <option value="3">Operador</option>
                            <option value="1">Administrador</option>
                        </select>
                    </div>
                    <div class="col-xs-12 Formulariobox-submit">
                        <input type="button" class="btn btn-success-ean btn-block" value="Crear" onclick="ValidaPersona()">
                    </div>
                </div>
            </div>
            <div class="col-xs-6">
                <div class="col-xs-12 TituloFormularioPersonalizado">
                    Administracion Personal
                </div>
                <div class="col-xs-8 FomularioPersonalizado">
                    <input type="text" class="form-control" placeholder="Cedula Personal" id="CedulaPersonal" />
                </div>
                <div class="col-xs-2 Formulariobox-submit" style="padding-top:0px">
                    <input type="button" class="btn btn-success-ean btn-block" value="B">
                </div>
                <div class="col-xs-2 Formulariobox-submit" style="padding-top:0px">
                    <input type="button" class="btn btn-success-ean btn-block" value="R">
                </div>
                <div class="col-xs-4 FomularioPersonalizado">
                    <input type="text" class="form-control" placeholder="Nombre" id="NombrePersonal" name="NombrePersonal" required>
                    </div>
                <div class="col-xs-4 FomularioPersonalizado">
                    <input type="text" class="form-control" placeholder="1er Apellido" id="ApellidoPersonal" name="ApellidoPersonal" required>
                </div>
                <div class="col-xs-4 FomularioPersonalizado">
                    <input type="text" class="form-control" placeholder="2do Apellido" id="Apellido2Personal" name="Apellido2Personal" required>
                </div>
                <div class="col-xs-12 FomularioPersonalizado">
                    <input type="text" class="form-control" placeholder="Usuario" id="UsuarioPersonal" name="UsuarioPersonal" required>
                </div>
                <div class="col-xs-6 FomularioPersonalizado">
                    <input type="password" class="form-control" placeholder="Contraseña" id="ContrasenaPersonal" name="ContrasenaPersonal" required>
                </div>
                <div class="col-xs-6 FomularioPersonalizado">
                    <input type="password" class="form-control" placeholder="Confirmar Contraseña" id="Contrasena2Personal" name="Contrasena2Personal" required>
                </div>
                <div class="col-xs-6 FomularioPersonalizado">
                    <select class="form-control" id="Activo" name="Activo">
                        <option value="1">Online</option>
                        <option value="0">Offline</option>
                    </select>
                </div>
                <div class="col-xs-6 FomularioPersonalizado">
                    <select class="form-control" id="TipoPersonal" name="TipoPersonal">
                        <option value="3">Operador</option>
                        <option value="1">Administrador</option>
                    </select>
                </div>
                <div class="col-xs-12 Formulariobox-submit">
                    <input type="button" class="btn btn-success-ean btn-block" value="Actualizar">
                </div>
            </div>
            <div class="col-xs-12">
                <div class="col-xs-12 TituloFormularioPersonalizado">Mini reporte del personal</div>
                <div class="col-xs-3"></div>
                <div class="col-xs-6">
                    <div class="col-xs-12 LetraMiniReporte">
                        Horas Con sesión abierta: <span id="HorasSesion"><b>0</b></span>
                        <br>
                        Cantidad de usuarios ingresados: <span id="UsuarioIngresado"><b>0 </b></span>
                        <br>
                        Fecha de creacion: <span id="FechaCreacion"><b><%: DateTime.Now.Year %> </b></span>
                        <br>
                    </div>
                </div>
                <div class="col-xs-3"></div>
            </div>
        </div>
    </div>
</asp:Content>
