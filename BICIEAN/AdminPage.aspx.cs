﻿using BICIEAN.Controlador.fachade;
using BICIEAN.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BICIEAN
{
    public partial class AdminPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static string ValidaPersona(int CedulaPersona)
        {
            return PersonaFachade.ValidaPersona(CedulaPersona);
        }

        [WebMethod]
        public static bool CrearPersona(string NombrePersona, string ApellidoPersona, string Apellido2Persona, string UsuarioPersona, string ContrasenaPersona, string CedulaPersona, string TipoPersona)
        {
            Persona persona = new Persona(Convert.ToInt32(CedulaPersona), NombrePersona, ApellidoPersona, Apellido2Persona, Convert.ToInt32(TipoPersona), UsuarioPersona, ContrasenaPersona, 1);
            return PersonaFachade.CrearPersona(persona);
        }
    }
}