﻿<%@ Page Title="Index Page" Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="BICIEAN.Index" %>

<!DOCTYPE html>
<html lang="en">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><%: Page.Title %> - BiciEAN</title>

    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/bundles/modernizr") %>
    </asp:PlaceHolder>

    <webopt:BundleReference runat="server" Path="~/Content/css" />
    <link href="~/favicon.ico" rel="shortcut icon" type="image/x-icon" />

</head>
<body>
    
    <form runat="server">
        <asp:ScriptManager runat="server">
            <Scripts>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="jquery" />
                <asp:ScriptReference Name="bootstrap" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>
    </form>
    <form>
        <form id="loginForm">
            <div class="row login">
                <div class="col-xs-12">
                    <div class="col-xs-12">
                        <img src="../Image/EANlog.png" style="height: 70px;"></img>
                    </div>
                        <div class="col-xs-12 loginbox-textbox">
                            <input type="text" class="form-control" placeholder="Username" id="txtUser" name="txtUser" />
                        </div>
                        <div class="col-xs-12 loginbox-textbox">
                            <input type="password" class="form-control" placeholder="Password" id="txtPassword" name="txtPassword" />
                        </div>
                    <div class="col-xs-12 loginbox-submit">
                        <input type="button" value="Login" class="btn btn-success-ean btn-block" onclick="login()">
                    </div>
                </div>
            </div>
        </form>
    </form>

    <script src="<%=ResolveClientUrl("../Scripts/loginScript.js") %>"" type="text/javascript"></script>
</body>
</html>
