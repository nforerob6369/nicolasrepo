﻿using BICIEAN.Controlador.fachade;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BICIEAN
{
    public partial class Index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static string ValidateUser(string Username, string Password)
        {
            DataTable dt = PersonaFachade.IngresarUsuario(Username, Password);
            string json = "error al logearse";
            if(dt != null)
            {
                json = JsonConvert.SerializeObject(dt, Formatting.Indented);
            }
            return json;
        }
    }
}