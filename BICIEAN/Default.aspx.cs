﻿using BICIEAN.Controlador.fachade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BICIEAN
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        
        [WebMethod]
        public static string CerrarSesion(string Cedula)
        {
            bool resp = PersonaFachade.CerrarSesion(Cedula);
            return resp.ToString();
        }
    }
}