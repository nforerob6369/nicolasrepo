﻿using BICIEAN.Controlador.dac;
using BICIEAN.Modelo;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace BICIEAN.Controlador.fachade
{
    public class PersonaFachade
    {
        public static DataTable IngresarUsuario(string Username, string Password)
        {
            PersonaDAC _PersonaDAC = new PersonaDAC();
            DataTable dtPersona = _PersonaDAC.ValidarUsuarioLogin(Username, Password);
            if(dtPersona != null)
            {
                string CedulaUser = dtPersona.Rows[0]["Cedula"].ToString();
                DataTable DtExisteLogin = _PersonaDAC.ValidarExistenciaLogin(CedulaUser);
                if(DtExisteLogin != null)
                {
                    if( Convert.ToInt32(DtExisteLogin.Rows[0]["Sesion"].ToString()) < 1)
                    {
                        if (_PersonaDAC.InsertSesion(CedulaUser))
                        {
                            DataColumn IdSesion = new DataColumn("SesionID");
                            dtPersona.Columns.Add(IdSesion);
                            dtPersona.Rows[0]["SesionID"] = _PersonaDAC.GetLastInsertSesionID().ToString();
                            return dtPersona;
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        _PersonaDAC.CerrarSesion(CedulaUser);
                        if (_PersonaDAC.InsertSesion(CedulaUser))
                        {
                            DataColumn IdSesion = new DataColumn("SesionID");
                            dtPersona.Columns.Add(IdSesion);
                            dtPersona.Rows[0]["SesionID"] = _PersonaDAC.GetLastInsertSesionID().ToString();
                            return dtPersona;
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        public static bool CerrarSesion(string CedulaUser)
        {
            PersonaDAC _PersonaDAC = new PersonaDAC();
            return _PersonaDAC.CerrarSesion(CedulaUser);
        }

        public static string ValidaPersona(int CedulaPersona)
        {
            PersonaDAC _PersonaDAC = new PersonaDAC();
            return _PersonaDAC.ValidaPersona(CedulaPersona);
        }

        public static bool CrearPersona(Persona persona)
        {
            PersonaDAC _PersonaDAC = new PersonaDAC();
            return _PersonaDAC.CrearPersona(persona);
        }
    }
}