﻿using BICIEAN.Modelo;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace BICIEAN.Controlador.dac
{
    public class PersonaDAC
    {
        #region "Sesion de usuario"
        public DataTable ValidarUsuarioLogin(string Username, string Password)
        {
            DataConection dataConection = new DataConection();
            string query = "SELECT Cedula,IdTipoPersona,concat_ws( ' ', Nombre, Apellido, Apellido2) AS Fullname FROM tblpersona WHERE Usuario = '" + Username + "' AND Password = '" + Password + "' AND Activo = 1 AND Usuario != '' ";
            DataTable dt = dataConection.SelectAsDataTable(query);
            return dt;
        }
        public bool InsertSesion(string CedulaUsuario)
        {
            DataConection dataConection = new DataConection();
            string query = "INSERT INTO tblpersonasesion (estado, CedulaUsuario) VALUES (1, " + CedulaUsuario + ")";
            bool InsertStatus = dataConection.RunSQLStatement(query);
            return InsertStatus;
        }
        public int GetLastInsertSesionID()
        {
            DataConection dataConection = new DataConection();
            string query = "SELECT Id FROM tblpersonasesion ORDER by Id DESC LIMIT 1";
            DataTable dt = dataConection.SelectAsDataTable(query);
            if (dt != null)
            {
                return Convert.ToInt32(dt.Rows[0]["Id"].ToString());
            }
            else
            {
                return -1;
            }
        }
        public DataTable ValidarExistenciaLogin(string CedulaUsuario)
        {
            DataConection dataConection = new DataConection();
            string query = "SELECT COUNT(*) Sesion FROM tblpersonasesion WHERE CedulaUsuario = "+ CedulaUsuario + " AND estado = 1";
            DataTable dt = dataConection.SelectAsDataTable(query);
            return dt;
        }
        public bool CerrarSesion(string CedulaUsuario)
        {
            DataConection dataConection = new DataConection();
            string UpdateStateAndLogoutQuery = "UPDATE tblpersonasesion SET estado = 0, FechaLogout = '"+ DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "' WHERE CedulaUsuario = "+ CedulaUsuario +" AND estado = 1";
            bool InsertStatus = dataConection.RunSQLStatement(UpdateStateAndLogoutQuery);
            if (InsertStatus)
            {
                string UpdateLogTimeQuery = "UPDATE tblpersonasesion SET tiempoLog=(TIMESTAMPDIFF(MINUTE, FechaLogin, FechaLogout))";
                InsertStatus = dataConection.RunSQLStatement(UpdateLogTimeQuery);
            }
            return InsertStatus;
        }
        #endregion
        #region "CRUD Usuario"
        public string ValidaPersona(int CedulaPersona)
        {
            DataConection dataConection = new DataConection();
            string Query = "Select count(*) AS Total From tblpersona WHERE Cedula = " + CedulaPersona + "";
            DataTable dt = dataConection.SelectAsDataTable(Query);
            if(dt != null)
            {
                if(Convert.ToInt32(dt.Rows[0]["Total"].ToString()) > 0)
                {
                    return "true"; //Existe ya una persona guardada con esta cedula
                }
                else
                {
                    return "false"; //No extiste una persona con esta Cedula
                }
            }
            else
            {
                return "error";
            }
        }
        public bool CrearPersona(Persona persona)
        {
            DataConection dataConection = new DataConection();
            string Query = "INSERT INTO tblpersona (CEDULA, NOMBRE, APELLIDO, APELLIDO2, IDTIPOPERSONA, USUARIO, PASSWORD, ACTIVO) VALUES("+persona.Cedula+", '"+persona.Nombre+"', '"+persona.Apellido+"', '"+persona.Apellido2+"', "+persona.IdTipoPersona+", '"+persona.Usuario+"', '"+persona.Password+"',  1)";
            bool resp = dataConection.RunSQLStatement(Query);
            return resp;
        }
        #endregion

    }
}