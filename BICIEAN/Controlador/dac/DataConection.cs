﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace BICIEAN.Controlador.dac
{
    public class DataConection
    {
        private static string connectionString = "datasource=127.0.0.1;port=3306;username=root;password=;database=biciean;";

        public  DataTable SelectAsDataTable(string Query)
        {
            MySqlConnection databaseConnection = new MySqlConnection(connectionString);
            MySqlCommand commandDatabase = new MySqlCommand(Query, databaseConnection);
            commandDatabase.CommandTimeout = 60;
            MySqlDataReader reader;
            DataTable dt = new DataTable();
            
            try
            {
                databaseConnection.Open();
                reader = commandDatabase.ExecuteReader();
                if (reader.HasRows)
                {
                    
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        DataColumn dc = new DataColumn(reader.GetName(i), typeof(String));
                        dt.Columns.Add(dc);
                    }
                    while (reader.Read())
                    {
                        DataRow dr = dt.NewRow();
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            dr[i] = reader.GetString(i);
                        }
                        //string[] row = { reader.GetString(0), reader.GetString(1), reader.GetString(2), reader.GetString(3) };
                        dt.Rows.Add(dr);
                    }
                }
                else
                {
                    dt = null;
                }
                databaseConnection.Close();
                return dt;
            }
            catch(Exception ex)
            {
                return null;
            }

            
        }

        public bool RunSQLStatement(string Query)
        {
            MySqlConnection databaseConnection = new MySqlConnection(connectionString);
            MySqlCommand commandDatabase = new MySqlCommand(Query, databaseConnection);
            commandDatabase.CommandTimeout = 60;
            MySqlDataReader reader;
            try
            {
                databaseConnection.Open();
                reader = commandDatabase.ExecuteReader();
                databaseConnection.Close();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }


        }
    }
}