﻿function onload() {
    if ("SesionData" in localStorage) {
        var DataSesion = localStorage.getItem("SesionData");
        if (DataSesion == "") {
            localStorage.setItem("SesionData", "");
            window.location = "/Vistas/Index.aspx";
        }
        DataSesion = jQuery.parseJSON(DataSesion);
        if (DataSesion.length > 0) {
            SetNameUser(DataSesion[0]["Fullname"]);
            ValidaTipoPersona(DataSesion[0]["IdTipoPersona"]);
        }
        else {
            localStorage.setItem("SesionData", "");
            window.location = "/Vistas/Index.aspx";
        }
        ValidaTipoPersona();
    }
    else {
        localStorage.setItem("SesionData", "");
        window.location = "/Vistas/Index.aspx";
    }
}
function SetNameUser(FullName) {
    $("#UserName").text(FullName);
}
function ValidaTipoPersona(TipoPersona) {
    switch (TipoPersona) {
        case "1": // Admin
            $("#SeccionAdmin").css("display", "block");
            break;
        case "3": // Operador
            $("#SeccionAdmin").css("display", "none");
            break;
    }
}