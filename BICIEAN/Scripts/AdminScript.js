﻿/*Comentario*/
function ValidaPersona() {
    var validate = validarCamposRegistro();
    if (validate) {
        var CedulaPersona = $("#CedulaPersona").val();
        $.ajax({
            type: "POST",
            url: "AdminPage.aspx/ValidaPersona",
            data: '{CedulaPersona : "' + CedulaPersona + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: crearPersona,
            failure: function (response) {
                alert(response.d);
            }
        });
    }
    else {
        alert("Porfavor diligenciar todos los campos");
    }
}

function crearPersona(response) {
    var resp = response.d;
    if (resp == "false") {
        var NombrePersona = $("#NombrePersona").val(); var ApellidoPersona = $("#ApellidoPersona").val(); var Apellido2Persona = $("#Apellido2Persona").val();
        var UsuarioPersona = $("#UsuarioPersona").val(); var ContrasenaPersona = $("#ContrasenaPersona").val(); var CedulaPersona = $("#CedulaPersona").val();
        var TipoPersona = $("#TipoPersona").val();
        $.ajax({
            type: "POST",
            url: "AdminPage.aspx/CrearPersona",
            data: '{NombrePersona : "' + NombrePersona + '", ApellidoPersona: "' + ApellidoPersona + '", Apellido2Persona: "' + Apellido2Persona + '" , UsuarioPersona: "' + UsuarioPersona + '", ContrasenaPersona: "' + ContrasenaPersona + '", CedulaPersona: "' + CedulaPersona + '", TipoPersona: "' + TipoPersona + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: PostCrearPersona,
            failure: function (response) {
                alert(response.d);
            }
        });
    }
    if (resp == "true") {
        alert("Ya existe un usuario con esa cedula");
        $("#CedulaPersona").val("");
    }
    if (resp == "error") {
        alert("Error al validar el usuario");
    }
}

function PostCrearPersona(response) {
    var resp = response.d;
    if (resp) {
        limpiarCamposRegistro();
        alert("Usuario creado correctamente!");
    }
    else {
        alert("Error al registrar el usuario \nContacte con su administrador.");
    }
    
}
function limpiarCamposRegistro() {
    $("#NombrePersona").val("");
    $("#ApellidoPersona").val("");
    $("#Apellido2Persona").val("");
    $("#UsuarioPersona").val("");
    $("#ContrasenaPersona").val("");
    $("#CedulaPersona").val("");
    $("#TipoPersona").val("");
    $("#Contrasena2Persona").val("");
}
function validarCamposRegistro() {
    var NombrePersona = $("#NombrePersona").val(); var ApellidoPersona = $("#ApellidoPersona").val(); var Apellido2Persona = $("#Apellido2Persona").val();
    var UsuarioPersona = $("#UsuarioPersona").val(); var ContrasenaPersona = $("#ContrasenaPersona").val(); var Contrasena2Persona = $("#Contrasena2Persona").val();
    var CedulaPersona = $("#CedulaPersona").val(); var TipoPersona = $("#TipoPersona").val();

    if (NombrePersona.trim() == "") return false;
    if (ApellidoPersona.trim() == "") return false;
    if (Apellido2Persona.trim() == "") return false;
    if (UsuarioPersona.trim() == "") return false;
    if (ContrasenaPersona.trim() == "") return false;
    if (Contrasena2Persona.trim() == "") return false;
    if (CedulaPersona.trim() == "") return false;
    if (TipoPersona.trim() == "") return false;
    if (ContrasenaPersona.trim() != Contrasena2Persona.trim()) {
        $("#ContrasenaPersona").val("");
        $("#Contrasena2Persona").val("");
        alert("La contraseña debe coincidir");
        return false;
    }
    return true;

}