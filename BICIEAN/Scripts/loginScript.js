﻿function login()
{
    var Username = $("#txtUser").val();
    var Password = $("#txtPassword").val();
    localStorage.setItem("SesionData", "");
    $.ajax({
        type: "POST",
        url: "Index.aspx/ValidateUser",
        data: '{Username : "' + Username + '", Password: "' + Password+'"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: PostLogin,
        failure: function (response) {
            alert(response.d);
        }
    });
}
function PostLogin(response)
{
    var resp = response.d;
    if (resp != "error al logearse") {
        localStorage.setItem("SesionData", resp);
        window.location = "../Default.aspx";
    }
    else {
        alert(response.d);
    }
}

function logout()
{
    var DataSesion = localStorage.getItem("SesionData");
    if (DataSesion == "") {
        window.location = "/Vistas/Index.aspx";
    }
    DataSesion = jQuery.parseJSON(DataSesion);
    var CedulaUsuario = DataSesion[0]["Cedula"];

    $.ajax({
        type: "POST",
        url: "Default.aspx/CerrarSesion",
        data: '{Cedula : "' + CedulaUsuario + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: PostLogout,
        failure: function (response) {
            alert(response.d);
        }
    });
}

function PostLogout(response) {
    var resp = response.d;
    if (resp == "True") {
        localStorage.setItem("SesionData", "");
        window.location = "/Vistas/Index.aspx";
    }
    else {
        alert("Algo fallo al cerrar la sesión");
    }
    
}