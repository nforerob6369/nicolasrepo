﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/BiciMasterPage.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="BICIEAN._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="col-xs-12" style="padding-left: 75px;">
        <div class="col-xs-12 InformationSection">
        <div class="col-xs-12" id="SeccionAdmin">
            <div class="col-xs-6">
                <div class="col-xs-3"></div>
                <div class="col-xs-2 AdminButtonSection" onclick="location.href='/AdminPage.aspx'">
                    <img src="Image/boss-64.png" title="Modo administrador"/>
                    <span class="LetraBotonPeque">Admin</span>
                </div>
                <div class="col-xs-7"></div>
            </div>
            <div class="col-xs-6"></div>
        </div>

        <div class="col-xs-12" id="SeccionBotones">
            <div class="col-xs-6 ButtonSection" id="SeccionIngreso" >
                <div class="col-xs-3"></div>
                <div class="col-xs-7 FondoBotonesSection" onclick="location.href='/IngresoPage.aspx'">
                    <img src="Image/bicycle-256.png" title="Ingresar Bici" />
                    <span class="LetraBotonGrande">Ingresar</span>
                </div>
                <div class="col-xs-2"></div>
            </div>
            <div class="col-xs-6 ButtonSection" id="SeccionRegistro" >
                <div class="col-xs-2"></div>
                <div class="col-xs-7 FondoBotonesSection" onclick="location.href='/RegistroPage.aspx'">
                    <img src="Image/register-256.png" title="Registrar Usuario" />
                    <span class="LetraBotonGrande">Registrar</span>
                </div>
                <div class="col-xs-3"></div>
            </div>
        </div>
    </div>
    </div>
</asp:Content>
