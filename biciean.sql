-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 08-01-2019 a las 00:16:00
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `biciean`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbldetallepersona`
--

CREATE TABLE `tbldetallepersona` (
  `ID` int(11) NOT NULL,
  `CodigoBici` varchar(50) NOT NULL,
  `CedulaCliente` int(50) NOT NULL,
  `FechaActualizacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Color` varchar(120) NOT NULL,
  `Marca` varchar(120) NOT NULL,
  `Observacion` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tbldetallepersona`
--

INSERT INTO `tbldetallepersona` (`ID`, `CodigoBici`, `CedulaCliente`, `FechaActualizacion`, `Color`, `Marca`, `Observacion`) VALUES
(1, '21121', 1032481707, '2019-01-06 22:00:55', 'Blanca', 'Biologica', 'Bici electrica');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblhistorico`
--

CREATE TABLE `tblhistorico` (
  `ID` int(11) NOT NULL,
  `CodigoBici` varchar(50) NOT NULL,
  `cedulaCliente` int(50) NOT NULL,
  `FechaIngreso` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `FechaSalida` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Estado` int(1) NOT NULL,
  `CedulaUsuario` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblmovingreso`
--

CREATE TABLE `tblmovingreso` (
  `CodigoBici` varchar(50) NOT NULL,
  `CedulaCliente` int(50) NOT NULL,
  `CedulaUsuario` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblpersona`
--

CREATE TABLE `tblpersona` (
  `Cedula` int(50) NOT NULL,
  `Nombre` varchar(120) NOT NULL,
  `Apellido` varchar(120) NOT NULL,
  `Apellido2` varchar(120) NOT NULL,
  `IdTipoPersona` int(11) NOT NULL,
  `Usuario` varchar(120) NOT NULL,
  `Password` varchar(120) NOT NULL,
  `FechaCreacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Activo` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tblpersona`
--

INSERT INTO `tblpersona` (`Cedula`, `Nombre`, `Apellido`, `Apellido2`, `IdTipoPersona`, `Usuario`, `Password`, `FechaCreacion`, `Activo`) VALUES
(123456789, 'Primer', 'Operador', 'Sistema', 3, 'operador', '123456', '2019-01-07 02:02:47', 1),
(1023957417, 'Lucas', 'Cubaque', 'Niño', 1, 'lcubaque', '123456', '2019-01-07 16:43:54', 1),
(1032481707, 'Nicolas', 'Forero', 'Bustamante', 1, 'nforerob', '123456', '2019-01-06 16:59:47', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblpersonasesion`
--

CREATE TABLE `tblpersonasesion` (
  `Id` int(11) NOT NULL,
  `FechaLogin` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `FechaLogout` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `estado` int(1) NOT NULL,
  `CedulaUsuario` int(50) NOT NULL,
  `tiempoLog` int(11) NOT NULL DEFAULT '0',
  `FechaLogActual` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tblpersonasesion`
--

INSERT INTO `tblpersonasesion` (`Id`, `FechaLogin`, `FechaLogout`, `estado`, `CedulaUsuario`, `tiempoLog`, `FechaLogActual`) VALUES
(24, '2019-01-06 22:56:12', '2019-01-07 01:28:33', 0, 1032481707, 152, '2019-01-07 06:28:33'),
(25, '2019-01-07 01:28:33', '2019-01-07 01:49:35', 0, 1032481707, 21, '2019-01-07 06:49:35'),
(26, '2019-01-07 01:52:19', '2019-01-07 01:52:22', 0, 1032481707, 0, '2019-01-07 06:52:22'),
(27, '2019-01-07 01:52:45', '2019-01-07 02:03:00', 0, 1032481707, 10, '2019-01-07 07:03:00'),
(28, '2019-01-07 02:03:07', '2019-01-07 02:05:07', 0, 123456789, 2, '2019-01-07 07:05:07'),
(29, '2019-01-07 13:17:14', '2019-01-07 13:32:00', 0, 1032481707, 14, '2019-01-07 18:32:00'),
(30, '2019-01-07 13:32:07', '2019-01-07 13:37:59', 0, 123456789, 5, '2019-01-07 18:37:59'),
(31, '2019-01-07 13:38:51', '2019-01-07 14:08:21', 0, 1032481707, 29, '2019-01-07 19:08:21'),
(32, '2019-01-07 14:08:21', '2019-01-07 16:01:07', 0, 1032481707, 112, '2019-01-07 21:01:08'),
(33, '2019-01-07 16:44:04', '2019-01-07 16:44:19', 0, 1023957417, 0, '2019-01-07 21:44:19'),
(34, '2019-01-07 16:44:49', '2019-01-07 16:45:52', 0, 1023957417, 1, '2019-01-07 21:45:52'),
(35, '2019-01-07 16:45:58', '2019-01-07 16:50:05', 0, 123456789, 4, '2019-01-07 21:50:05'),
(36, '2019-01-07 16:50:14', '2019-01-07 16:53:34', 0, 123456789, 3, '2019-01-07 21:53:34'),
(37, '2019-01-07 16:53:11', '2019-01-07 16:54:01', 0, 1032481707, 0, '2019-01-07 21:54:01'),
(38, '2019-01-07 16:54:01', '2019-01-07 16:54:22', 0, 1032481707, 0, '2019-01-07 21:54:22'),
(39, '2019-01-07 16:54:53', '2019-01-07 16:54:53', 1, 1032481707, 0, '2019-01-07 21:54:53');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbltipopersona`
--

CREATE TABLE `tbltipopersona` (
  `IdTipoPersona` int(11) NOT NULL,
  `Descripcion` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tbltipopersona`
--

INSERT INTO `tbltipopersona` (`IdTipoPersona`, `Descripcion`) VALUES
(1, 'Administrador'),
(2, 'Cliente'),
(3, 'Operador');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tbldetallepersona`
--
ALTER TABLE `tbldetallepersona`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `CodigoBici` (`CodigoBici`),
  ADD KEY `CedulaCliente` (`CedulaCliente`);

--
-- Indices de la tabla `tblhistorico`
--
ALTER TABLE `tblhistorico`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `CodigoBici` (`CodigoBici`,`cedulaCliente`,`CedulaUsuario`),
  ADD KEY `CodigoBici_2` (`CodigoBici`),
  ADD KEY `cedulaCliente` (`cedulaCliente`),
  ADD KEY `CedulaUsuario` (`CedulaUsuario`);
ALTER TABLE `tblhistorico` ADD FULLTEXT KEY `CodigoBici_3` (`CodigoBici`);

--
-- Indices de la tabla `tblmovingreso`
--
ALTER TABLE `tblmovingreso`
  ADD PRIMARY KEY (`CodigoBici`),
  ADD KEY `CedulaCliente` (`CedulaCliente`,`CedulaUsuario`),
  ADD KEY `CedulaUsuario` (`CedulaUsuario`);

--
-- Indices de la tabla `tblpersona`
--
ALTER TABLE `tblpersona`
  ADD PRIMARY KEY (`Cedula`),
  ADD KEY `IdTipoPersona` (`IdTipoPersona`);

--
-- Indices de la tabla `tblpersonasesion`
--
ALTER TABLE `tblpersonasesion`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `CedulaUsuario` (`CedulaUsuario`);

--
-- Indices de la tabla `tbltipopersona`
--
ALTER TABLE `tbltipopersona`
  ADD PRIMARY KEY (`IdTipoPersona`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tbldetallepersona`
--
ALTER TABLE `tbldetallepersona`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `tblhistorico`
--
ALTER TABLE `tblhistorico`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tblpersonasesion`
--
ALTER TABLE `tblpersonasesion`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT de la tabla `tbltipopersona`
--
ALTER TABLE `tbltipopersona`
  MODIFY `IdTipoPersona` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `tbldetallepersona`
--
ALTER TABLE `tbldetallepersona`
  ADD CONSTRAINT `tbldetallepersona_ibfk_1` FOREIGN KEY (`CedulaCliente`) REFERENCES `tblpersona` (`Cedula`);

--
-- Filtros para la tabla `tblhistorico`
--
ALTER TABLE `tblhistorico`
  ADD CONSTRAINT `tblhistorico_ibfk_1` FOREIGN KEY (`cedulaCliente`) REFERENCES `tblpersona` (`Cedula`),
  ADD CONSTRAINT `tblhistorico_ibfk_2` FOREIGN KEY (`CedulaUsuario`) REFERENCES `tblpersona` (`Cedula`),
  ADD CONSTRAINT `tblhistorico_ibfk_3` FOREIGN KEY (`CodigoBici`) REFERENCES `tbldetallepersona` (`CodigoBici`);

--
-- Filtros para la tabla `tblmovingreso`
--
ALTER TABLE `tblmovingreso`
  ADD CONSTRAINT `tblmovingreso_ibfk_1` FOREIGN KEY (`CedulaCliente`) REFERENCES `tblpersona` (`Cedula`),
  ADD CONSTRAINT `tblmovingreso_ibfk_2` FOREIGN KEY (`CedulaUsuario`) REFERENCES `tblpersona` (`Cedula`),
  ADD CONSTRAINT `tblmovingreso_ibfk_3` FOREIGN KEY (`CodigoBici`) REFERENCES `tbldetallepersona` (`CodigoBici`);

--
-- Filtros para la tabla `tblpersona`
--
ALTER TABLE `tblpersona`
  ADD CONSTRAINT `tblpersona_ibfk_1` FOREIGN KEY (`IdTipoPersona`) REFERENCES `tbltipopersona` (`IdTipoPersona`);

--
-- Filtros para la tabla `tblpersonasesion`
--
ALTER TABLE `tblpersonasesion`
  ADD CONSTRAINT `tblpersonasesion_ibfk_1` FOREIGN KEY (`CedulaUsuario`) REFERENCES `tblpersona` (`Cedula`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
